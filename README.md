# README
There is a lot more functionality that's needed. I am still working on it. Any feedback would be welcome. If you could point to a line of code that code be improved and a way to improve it that would be appreciated.

# There are four notable things in this application.
1. Authentication is custom. Authorization is handled with cancancan.
2. Images are securely handled using send_file and carrierwave.
3. Searches use separate classes to cut down on code and increase reusability.
4. Slack bot notifications can be enabled
# Installation
###
* Follow this guide for base setup instructions
* https://gorails.com/setup/ubuntu/16.04
* ruby 2.4.0p0 (2016-12-24 revision 57164) [x86_64-linux]
* carrierwave dependencies
* sudo apt-get install imagemagick libmagickwand-dev
* Current db is sqlite, but can easily run mysql or psql
* If you want to use psql comment line 15 and uncomment line 16
* in app/models/concerns/filterable.rb
* and of course adjust the gem file and database.yml file
# Slack
* To enable the slack bot notifier, add an enviroment key
* and uncomment line 45 in the tasks controller
* @task.send_slack  
# Environment Variables
* needed environment variables can be found at .rbenv-vars
###
# rbenv environment variables can be used
###
* cd ~/.rbenv/plugins
* cd ~
* git clone https://github.com/sstephenson/rbenv-vars.git
* cd ~/helpdesk
* view .rbenv-vars for environment variables
###
* When booting up the application for the first time
* rake db:create db:migrate db:seed
* login with email harry.fairbanks@test.com pw password for admin role
* login with email bob@test.com pw password for business role
###
# Tests
* bundle exec rspec
###
