 /*exported preview_avatar, cancel_avatar_upload */

function preview_avatar(e){
  var input = document.getElementById(e.id);

  if ( input.files.item(0).size < 5100000)
    {
      document.getElementById("cancel_button").style.display = "inline";
      var fReader = new FileReader();
      fReader.readAsDataURL(input.files[0]);
      fReader.name = input.files[0].name;
      fReader.type = input.files[0].type;
      fReader.onloadend = function(event){
      var img = document.getElementById("avatar_img");
      img.src = event.target.result;
    };
    }
  else
    {
      document.getElementById('file_input_avatar').value = '';
      window.alert('Avatar must be less then 5 mb.');
    }
}

function cancel_avatar_upload(){
  document.getElementById("cancel_button").style.display = 'none';
  var img = document.getElementById('avatar_img');
  img.src = document.getElementById('original_avatar_source').innerHTML.trim();
}
