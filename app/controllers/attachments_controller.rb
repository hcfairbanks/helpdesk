class AttachmentsController < ApplicationController
  include ReadableErrors
  load_and_authorize_resource
  before_action :set_attachment, only: [:destroy]

  def serve_thumb
    send_file(@attachment.doc.thumb_path,
              disposition: "inline",
              x_sendfile: true) || return_failsafe
  end

  def serve_doc
    send_file(@attachment.doc.doc_path, disposition: "inline", x_sendfile: true) || return_failsafe
  end

  def return_failsafe
    assets_images_path = File.join(Rails.root, "app", "assets", "images")
    send_file(File.join(assets_images_path, "no_doc_thumb.png"),
              disposition: "inline",
              x_sendfile: true)
  end

  # DELETE /attachments/1
  # DELETE /attachments/1.json
  def destroy
    task = Task.find(@attachment.task_id)
    respond_to do |format|
      if @attachment.destroy
        format.html { redirect_to(task) }
        format.json { head :no_content }
      else
        format.html { redirect_to(task, alert: formated_errors(@attachment)) }
        format.json { render json: task.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def set_attachment
    @attachment = Attachment.find(params[:id])
  end
end
