class PasswordResetsController < ApplicationController
  include ReadableErrors
  before_action :get_user, only: [:edit, :update]
  before_action :check_expiration, only: [:edit, :update]

  def new
  end

  def create
    @user = User.find_by(email: params[:password_reset][:email].downcase)
    if @user
      @user.create_reset_digest
      @user.send_password_reset_email
    end
    flash.now[:notice] = t("password_reset.email_sent")
    redirect_to root_url, notice: t("password_reset.email_sent")
  end

  def update
    if params[:user][:password].empty?
      @user.errors.add(:password, t("password_reset.empty"))
      flash.now[:alert] = formated_errors(@user)
      render :edit
    elsif @user.update_attributes(user_params)
      redirect_to root_url, notice: t("password_reset.reset")
    else
      flash.now[:alert] = formated_errors(@user)
      render :edit
    end
  end

  def edit
  end

  private
  # Checks expiration of reset token.
  def check_expiration
    if @user.password_reset_expired?
      flash.now[:alert] = t("password_reset.reset_expired")
      redirect_to new_password_reset_url
    end
  end

  def user_params
    params.require(:user).permit(:password, :password_confirmation)
  end

  def get_user
    @user = User.find_by(email: params[:email])
  end
end
