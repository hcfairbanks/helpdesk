class SessionsController < ApplicationController
  def new
  end

  def create
    user = User.find_by(email: params[:email])
    if user && user.authenticate(params[:password]) && user.role.name != Role::UNASSIGNED
      session[:user_id] = user.id
      redirect_to tasks_url, notice: t("session.logged_in")
    else # TODO: add case for email found but not authorized
      flash.now.alert = t("session.invalid")
      render :new
    end
  end

  def destroy
    session[:user_id] = nil
    redirect_to root_url, notice: t("session.logged_out")
  end
end
