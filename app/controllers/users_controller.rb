class UsersController < ApplicationController
  include ReadableErrors
  include UsersHelper
  load_and_authorize_resource
  before_action :set_user, only: [:show,
                                  :edit,
                                  :update,
                                  :destroy,
                                  :serve_small,
                                  :serve_medium,
                                  :serve_large]

  def serve_small
    send_file(@user.avatar.small_path, disposition: "inline", x_sendfile: true) || return_failsafe
  end

  def serve_medium
    send_file(@user.avatar.medium_path, disposition: "inline", x_sendfile: true) || return_failsafe
  end

  def serve_large
    send_file(@user.avatar.large_path, disposition: "inline", x_sendfile: true) || return_failsafe
  end

  def index
    @users = User.all.where.not(email: User::UNASSIGNED_USER)
    @users = @users.filter_like(params.slice(:first_name, :last_name, :email))
    @users = @users.filter_exact(params.slice(:role))
    @users = @users.order(params[:order_by]) if !params[:order_by].blank?
    @users = @users.paginate(page: params[:page], per_page: 10)
  end

  def show
  end

  def new
    @user = User.new
  end

  def edit
  end

  def create
    @user = User.new(user_params)
    if @user.save
      redirect_to create_url[:url], notice: create_url[:msg]
    else
      render :new, alert: formated_errors(@user)
    end
  end

  def update
    if @user.update(pw_present(user_params))
      redirect_to @user, notice: t("user.updated")
    else
      render :edit
    end
  end

  def destroy
    if @user.destroy
      destroyed_current_user?(@user)
      redirect_to destroy_url[:url], notice: destroy_url[:msg]
    else
      render :new, alert: formated_errors(@user)
    end
  end

  def destroy_avatar
    if params[:id]
      if @user.avatar
        @user.remove_avatar = true
        @user.save
        FileUtils.rmdir(File.join(Rails.root,
                                  "dynamic_files",
                                  Rails.env,
                                  "user",
                                  "avatar",
                                  params[:id]))
      end
    end
    redirect_to(@user)
  end

  private

  def pw_present(old_params)
    if old_params[:password].blank? && old_params[:password_confirmation].blank?
      old_params.delete(:password)
      old_params.delete(:password_confirmation)
    end
    old_params
  end

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    permited_params = [:id,
                       :avatar,
                       :first_name,
                       :last_name,
                       :password,
                       :password_confirmation,
                       :email,
                       :role_id]
    if !current_user.is_admin?
      permited_params = permited_params.delete(:role_id)
    end
    params.require(:user).permit(permited_params)
  end
end
