class Attachment < ApplicationRecord
  after_destroy :remove_attachment_directory

  mount_uploader :doc, DocUploader, dependent: :destroy
  validates_presence_of :doc
  validates_size_of :doc, maximum: 25.megabyte, message: I18n.t("attachment.limit")

  belongs_to :task
  validates_associated :task, presence: true

  protected

  def remove_attachment_directory
    FileUtils.remove_dir(File.join(Rails.root,
                                   "dynamic_files",
                                   Rails.env,
                                   "attachment",
                                   "doc",
                                   id.to_s), force: true)
  end
end
