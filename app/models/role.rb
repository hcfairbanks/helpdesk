class Role < ApplicationRecord
  ADMIN      = "admin"
  BUSINESS   = "business"
  UNASSIGNED = "unassigned"

  has_many :users, dependent: :restrict_with_error
  validates :name, presence: true
end
