class User < ApplicationRecord
  attr_accessor :remember_token, :activation_token, :reset_token
  has_secure_password

  include Filterable
  UNASSIGNED_USER = "unassigned@test.com"
  NO_AVATAR_THUMB = "no_avatar_image_thumb.jpg"

  has_many :assignments, class_name: "Task", foreign_key: "assigned_to_id", dependent: :restrict_with_error
  has_many :reports,     class_name: "Task", foreign_key: "reported_by_id", dependent: :restrict_with_error
  has_many :comments, dependent: :restrict_with_error

  after_create :assign_default_role
  after_destroy :remove_attachment_directory

  mount_uploader :avatar, AvatarUploader, dependent: :destroy
  validates_size_of :avatar, maximum: 5.megabyte, message: I18n.t("avatar.limit")

  belongs_to :role

  validates :first_name, presence: { message: I18n.t("user.letters") }, format: { with: /[a-zA-z]/ }
  validates :last_name, presence: { message: I18n.t("user.letters") }, format: { with: /[a-zA-z]/ }
  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i }, uniqueness: true

  # Sets the password reset attributes.
   def create_reset_digest
     self.reset_token = self.new_token
     update_attribute(:reset_digest,  self.digest(reset_token))
     update_attribute(:reset_sent_at, Time.zone.now)
   end

   # Sends password reset email.
   def send_password_reset_email
     UserMailer.password_reset(self).deliver_now
   end

   # Returns true if a password reset has expired.
  def password_reset_expired?
    self.reset_sent_at < 2.hours.ago
  end

  def is_admin?
    answer = false
    if !role.blank? && role.name == Role::ADMIN
      answer = true
    end
    answer
  end

  def is_business?
    answer = false
    if !role.blank? && role.name == Role::BUSINESS
      answer = true
    end
    answer
  end

  def assign_default_role
    self.role ||= Role.find_by_name("unassigned")
    self.save
  end

  def self.authenticate(email, password) # TODO: FIX this
    user = find_by_email(email)
    if user && user.password_hash == BCrypt::Engine.hash_secret(password, user.password_salt)
      user
    else
      nil
    end
  end

  def encrypt_password
    if password.present?
      self.password_salt = BCrypt::Engine.generate_salt
      self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
    end
  end

  def new_token
    SecureRandom.urlsafe_base64
  end

  def digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

  protected

  def remove_attachment_directory
    FileUtils.remove_dir(File.join(Rails.root,
                                   "dynamic_files",
                                   Rails.env,
                                   "user",
                                   "avatar",
                                   id.to_s), force: true)
  end
end
