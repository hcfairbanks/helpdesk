class DocUploader < CarrierWave::Uploader::Base
  include CarrierWave::RMagick
  storage :file

  def store_dir
    File.join(Rails.root,
              "dynamic_files",
              Rails.env,
              model.class.to_s.underscore.to_s,
              mounted_as.to_s,
              model.id.to_s)
  end

  def image?
    image_types = [".png", ".jpeg", ".jpg", ".gif"]
    image_types.include? File.extname(model.doc_identifier).downcase
  end

  def thumb_path
    if image?
      File.join(store_dir, "thumb_#{model.doc_identifier}")
    else
      File.join(Rails.root, "app", "assets", "images", "doc_thumb.jpg")
    end
  end

  def doc_path
    File.join(store_dir, model.doc_identifier)
  end

  def thumb_url
    File.join(base_url, "serve_thumb")
  end

  def doc_url
    File.join(base_url, "serve_doc")
  end

  def base_url
    File.join(ENV["ROOT_URL"], model.id.to_s)
  end

  version :thumb, if: :process_image? do
    process resize_to_fill: [50, 50]
  end

  protected

  def process_image?(new_file)
    new_file.content_type.include? "image"
  end
end
