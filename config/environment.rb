# Load the Rails application.
require_relative "application"

# Initialize the Rails application.
Rails.application.initialize!

ActionMailer::Base.smtp_settings = {
  address:              ENV["SMTP_ADDRESS"],
  port:                 ENV["SMTP_PORT"],
  domain:               ENV["SMTP_DOMAIN"],
  user_name:            ENV["SMTP_USER_NAME"],
  password:             ENV["SMTP_PASSWORD"],
  authentication:       :plain,
  enable_starttls_auto: true
}

Slack.configure do |config|
  config.token = ENV["SLACK_API_TOKEN"]
end
