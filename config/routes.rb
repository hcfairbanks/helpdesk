Rails.application.routes.draw do
  resources :comments
  resources :statuses
  resources :priorities
  resources :verticals
  resources :request_types
  resources :roles
  resources :tasks
  resources :sessions, only: [:new, :create, :destroy]
  resources :password_resets, only: [:new, :create, :edit, :update]
  get "password_resets/edit"

  root "sessions#new"

  get "signup", to: "users#new", as: "signup"
  get "login", to: "sessions#new", as: "login"
  get "logout", to: "sessions#destroy", as: "logout"

  resources :users do
    member do
      get "/serve_small",  action: "serve_small"
      get "/serve_medium", action: "serve_medium"
      get "/serve_large",  action: "serve_large"
    end
  end

  get ":id/serve_doc", to: "attachments#serve_doc"
  get ":id/serve_thumb", to: "attachments#serve_thumb"

  get "/delete_avatar", to: "users#destroy_avatar" # TODO: Fix this
  get "/delete_doc", to: "attachments#destroy" # TODO: Fix this
end
