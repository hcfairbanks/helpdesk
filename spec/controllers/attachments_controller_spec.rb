require "rails_helper"
require "spec_helper"

RSpec.describe AttachmentsController, type: :controller do
  before(:each) { @admin = admin_login }

  let!(:task_object) { FactoryGirl.create(:task, reported_by: @admin) }

  let(:valid_attributes) {
    {
      task: task_object,
      doc: fixture_file_upload("/binaries/headshots/2.jpg", "image/jpg", "true")
    }
  }
  let(:valid_file) {
    {
      task: task_object,
      doc: fixture_file_upload("/binaries/test_files/sample.csv",
                               "text/csv",
                               "true")
    }
  }

  let(:invalid_attributes) {
    {
      doc: "stuff"
    }
  }

  describe "GET #serve_doc" do
    it "response with thumbnail for image" do
      attachment_object = Attachment.create valid_attributes
      get :serve_thumb, params: { id: attachment_object.to_param }
      expect(response.header["Content-Type"]).to eq("image/jpeg")
      expect(response.header["Content-Disposition"]).to eq("inline; filename=\"thumb_2.jpg\"")
      expect(controller.headers["Content-Transfer-Encoding"]).to eq("binary")
    end
    it "response with doc image" do
      attachment_object = Attachment.create valid_attributes
      get :serve_doc, params: { id: attachment_object.to_param, type: "doc" }
      expect(response.header["Content-Type"]).to eq("image/jpeg")
      expect(response.header["Content-Disposition"]).to eq("inline; filename=\"2.jpg\"")
      expect(controller.headers["Content-Transfer-Encoding"]).to eq("binary")
    end
    it "response with doc csv" do
      attachment_object = Attachment.create valid_file
      get :serve_doc, params: { id: attachment_object.to_param }
      expect(response.header["Content-Type"]).to eq("text/csv")
      expect(response.header["Content-Disposition"]).to eq("inline; filename=\"sample.csv\"")
      expect(controller.headers["Content-Transfer-Encoding"]).to eq("binary")
    end
    it "response with generic thumbnail for csv" do
      attachment_object = Attachment.create valid_file
      get :serve_thumb, params: { id: attachment_object.to_param }
      expect(response.header["Content-Type"]).to eq("image/jpeg")
      expect(response.header["Content-Disposition"]).to eq("inline; filename=\"doc_thumb.jpg\"")
      expect(controller.headers["Content-Transfer-Encoding"]).to eq("binary")
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested attachment" do
      attachment_object = Attachment.create(valid_attributes)
      expect {
        delete :destroy, params: { id: attachment_object.to_param }
      }.to change(Attachment, :count).by(-1)
    end
    it "redirects to the task" do
      attachment_object = Attachment.create(valid_attributes)
      delete :destroy, params: { id: attachment_object.to_param }
      expect(response).to redirect_to(task_object)
    end
  end
end
