require "rails_helper"
require "spec_helper"

RSpec.describe PasswordResetsController, type: :controller do
  before(:each) {
    @admin = admin_login
    ActionMailer::Base.deliveries.clear
  }

  it "GET #new" do
    get :new, params: {}
    expect(response).to have_http_status(:ok)
  end

  describe "GET #edit" do
    it "response is ok" do
      @admin.reset_sent_at = DateTime.now
      @admin.save
      get :edit, params: { email: @admin.email }
      expect(response).to have_http_status(:ok)
    end
    it "alerts that password has expired" do
      @admin.reset_sent_at = DateTime.now - 2.hours
      @admin.save
      get :edit, params: { email: @admin.email }
      expect(flash[:alert]).to match("Password reset has expired.")
    end
  end

  describe "POST #create" do
    it "sends reset email" do
      post :create, params: { password_reset: { email: @admin.email } }
      expect(ActionMailer::Base.deliveries.count).to eq(1)
    end
    it "has correct email attributes" do
      post :create, params: { password_reset: { email: @admin.email } }
      expect(ActionMailer::Base.deliveries.first.subject).to eq("Password Reset")
      expect(ActionMailer::Base.deliveries.first.parts.first.body.raw_source).to include("To reset your password click the link below:")
      expect(ActionMailer::Base.deliveries.first.parts.first.body.raw_source).to include("This link will expire in two hours.")
      expect(ActionMailer::Base.deliveries.first.parts.first.body.raw_source).to include("If you did not request your password to be reset, please ignore this email and")
      expect(ActionMailer::Base.deliveries.first.parts.first.body.raw_source).to include("This link will expire in two hours.")
    end
    it "redirects to root_url" do
      post :create, params: { password_reset: { email: @admin.email } }
      expect(response).to redirect_to(root_url)
    end
    it "displays the correct flash message" do
      post :create, params: { password_reset: { email: @admin.email } }
      expect(flash[:notice]).to match("Email sent with password reset instructions")
    end
  end
end
