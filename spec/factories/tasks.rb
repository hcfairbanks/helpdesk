FactoryGirl.define do
  factory :task do
    title "test task"
    description "test description"
    reported_by_id :user
    assigned_to_id :user
    status
    request_type
    member_facing true
    vertical
    link "www.testlink.com"
    required_by Date.today
    priority
  end
end
