require "rails_helper"

RSpec.describe Task, type: :model do
  let(:user_role) { FactoryGirl.build(:role, { name: "user" }) }
  let(:user_bob) { FactoryGirl.build(:user) }

  let(:valid_attributes) {
    {
      reported_by: user_bob,
      description: "Some task."
    }
  }

  let(:valid_attachment_attributes) {
    {
      reported_by: user_bob,
      description: "Some task.",
      attachments_attributes: [
        { doc: Rack::Test::UploadedFile.new(
          File.open(File.join(Rails.root, "/spec/fixtures/binaries/headshots/2.jpg"
        )))}]
    }
  }

  let(:no_reporter_attributes) {
    {
      description: "Some task.",
      attachments_attributes: [
        { doc: Rack::Test::UploadedFile.new(
          File.open(File.join(Rails.root, "/spec/fixtures/binaries/headshots/2.jpg"
        )))}]
    }
  }

  let(:no_description_attributes) {
    {
      reported_by: user_bob,
      attachments_attributes: [
        { doc: Rack::Test::UploadedFile.new(
          File.open(File.join(Rails.root, "/spec/fixtures/binaries/headshots/2.jpg"
          )))}]
    }
  }

  describe "creation" do
    it "is valid with reporter and description" do
      task_object = Task.create(valid_attributes)
      expect(task_object.reported_by.first_name).to eq("Bob")
      expect(task_object.description).to eq("Some task.")
      expect(task_object).to be_valid
    end
    it "is valid with attachment" do
      task_object = Task.create(valid_attachment_attributes)
      expect(task_object.attachments.count).to eq(1)
      expect(task_object).to be_valid
    end
    it "is invalid without reporter" do
      task_object = Task.create(no_reporter_attributes)
      expect(task_object).to be_invalid
    end
    it "is invalid without description" do
      task_object = Task.create(no_reporter_attributes)
      expect(task_object).to be_invalid
    end
  end
end
